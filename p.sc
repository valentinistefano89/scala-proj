def printName(first: String, last: String): Unit = {
  println(first + " " + last)
}

printName("John", "Smith")  // Prints "John Smith"
printName(first = "John", last = "Smith")  // Prints "John Smith"
printName(last = "Smith", first = "John")  // Prints "John Smith"

printName("Smith", "John") // Prints "Smith John" - non sono specificati i metodi "first" e "last"

def sumInts(a: Int, b: Int): Int =
  if(a > b) 0 else a + sumInts(a + 1, b)
