//DO WHILE LOOP

object ScalaTutorial {
  def main(args:Array[String]): Unit =
  {
    var i = 0

    while(i <= 10){
      println(+ i)
      i += 1
    }
  }
}

// DO WHILE LOOP
object ScalaTutorial2 {
  def main(args:Array[String]): Unit =
  {
    var i = 0

    do{
        println(+ i)
        i += 1
    } while (i != 20)
  }
}

// FOR LOOP
object ScalaTutorial3 {
  def main(args:Array[String]): Unit =
  {
    var i = 0

    for(i <-1 to 11)
      println(+ i)
  }
}

object ScalaTutorial4 {
  def main(args:Array[String]): Unit =
  {
    var i = 0
    val randLetters = "ABCDEFGHILMNOPQRSTUVZ"
    for(i <- 0 until randLetters.length)
      println(randLetters(i))
  }
}