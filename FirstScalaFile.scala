// Objects - objects have properties and behaviors.
// For example: a dog-like attributes: color, name, behavior: call, run, eat and so on.
// Object is an instance of a class.

// Class - class is an abstract object, and the object is a specific instance of a class.
// Classes in Scala are blueprints for creating objects.
// They can contain methods, values, variables, types, objects, traits, and classes which are collectively called members.

// Method - the method described in the basic act, a class can contain multiple methods.

// Fields - Each object has its own unique set of instance variables, namely fields.
// Properties of the object through the field assignment to be created.



////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//CLASSI
//Scala è un linguaggio orientato agli oggetti e come tale presenta il concetto di classe.
//Le classi in Scala sono dichiarate usando una sintassi molto simile a quella usata in Java.
//Un’importante differenza è che le classi in Scala possono avere dei parametri.

class Complex(real: Double, imaginary: Double) {
  def re()= real
  def im() = imaginary
}

//Questa classe per i numeri complessi prende due argomenti:
//(1)la parte immaginaria e (2)quella reale del numero complesso.
//I due argomenti sono richiamati nell'object ComplexNumbers.
//Un problema dei metodi "re" e "im" è che, per essere invocati, è necessario far seguire
// il nome del metodo da una coppia di parentesi tonde vuote
object ComplexNumbers {
  def main(args: Array[String]) {
    val c = new Complex(1.2, 3.4)  //val è una variabile non modificabile
    var d = new Complex(1.5, 6)  //var è una variabile modificabile
    println("imaginary part: " + c.im())
    println("real part: " +  c.re)
    println("imaginary part: " + d.im())
    println("real part: " +  d.re)
  }
}


////////////////////////////////////////////////////////////////////////
object ComplexNumbers2 {
  def main(args: Array[String]) {
    val c = new Complex(99, 999)
    var d = new Complex(15, 60)
    println("imaginary part: " + c.im())
    println("real part: " +  c.re)
    println("imaginary part: " + d.im())
    println("real part: " +  d.re)
  }
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//In Scala anche le funzioni sono oggetti. È pertanto possibile passare le funzioni come argomenti, memorizzarle in variabili e ritornarle da altre funzioni.
// L’abilità di manipolare le funzioni come valori è uno dei punti cardini di un interessante paradigma di programmazione chiamato programmazione funzionale.

//Come esempio semplice del perché può risultare utile usare le funzioni come valori consideriamo una
// funzione timer che deve eseguire delle azione ogni secondo.
object Timer {  //la funzione timer è chiamata oncePerSecond
  def oncePerSecond(callback: () => Unit) {  //Unit è il tipo di tutte le funzioni che non prendono nessun argomento e non restituiscono niente (il tipo Unit è simile al void
    while (true) { callback(); Thread sleep 1000 }
  }
  def timeFlies() {
    println("time flies like an arrow...")
  }
  def main(args: Array[String]) {
    oncePerSecond(timeFlies)
  }
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//EREDITARIETA'
// In informatica l'ereditarietà è uno dei concetti fondamentali nel paradigma di programmazione a oggetti.
// Essa consiste in una relazione che il linguaggio di programmazione, o il programmatore stesso, stabilisce tra due classi.
// Se la classe B eredita dalla classe A, si dice che B è una sottoclasse di A e che A è una superclasse di B.



////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
///OVERRIDE
//In Scala tutte le classi sono figlie di una super-classe.
// Quando nessuna super-classe viene specificata, come nell’esempio della classe Complex,
// la classe scala.AnyRef è implicitamente usata.
//In Scala è possibile eseguire la sovrascrittura (override) dei metodi ereditati dalla super-classe.
// È pertanto necessario specificare esplicitamente il metodo che si sta sovrascrivendo usando il modificatore override per evitare sovrascritture accidentali.
// Come esempio estendiamo la nostra classe Complex ridefinendo il metodo toString ereditato da Object.


class Complexx(real: Double, imaginary: Double)
{
  def re() = real
  def im() = imaginary
  override def toString() =
    "" + re + (if (im < 0) "" else "+") + im + "i"
}

object ComplexNumbers6 {
  def main(args: Array[String]) {
    val c = new Complexx(91, 991)
    var d = new Complexx(11, 61)
    println("imaginary part: " + c.im())
    println("real part: " +  c.re)
    println("imaginary part: " + d.im)
    println("real part: " +  d.re)
  }
}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//DATAFRAME

import java.text.DateFormat._
import java.util.{Date, Locale}

// {@code DateFormat} is an abstract class for date/time formatting subclasses which
// formats and parses dates or time in a language-independent manner.

object FrenchDate {
  def main(args: Array[String]) {
    val today = new Date
    val df = getDateInstance(LONG, Locale.FRANCE)
    println(df format today)
  }
}



////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//CASE CLASSES + PATTERN MATCHING + TREES
// Un tipo di struttura dati che spesso si trova nei programmi è l’albero.
// Ad esempio, gli interpreti ed i compilatori abitualmente rappresentano i programmi internamente come alberi.
// Esamineremo ora come gli alberi sono rappresentati e manipolati in Scala attraverso un semplice programma calcolatrice.
// Lo scopo del programma è manipolare espressioni aritmetiche molto semplici composte da somme, costanti intere e variabili intere.
// Due esempi di tali espressioni sono 1+2 e (x+x)+(7+y).
// L'albero è il più naturale tipo di rappresentazione per le espressioni, con i nodi che rappresentano le operazioni
// (nel nostro caso, l’addizione) mentre le foglie sono i valori (costanti o variabili).

// L'albero è abitualmente rappresentato usando una super-classe astratta per gli alberi e
// una concreta sotto-classe per i nodi() o le foglie.

abstract class Treee
case class Sum(l: Treee, r: Treee) extends Treee  //Scala fornisce il concetto di classi case (case classes) che è qualcosa che si trova nel mezzo delle due rappresentazioni.
case class Var(n: String) extends Treee
case class Const(v: Int) extends Treee

//Ora un object che contiene due funzioni: la prima spiega l'espressione voluta
//la seconda
object TreeeOperations {
  type Environment = String => Int
  //Environment = { case "x" => 5 case "y" => 7}

  def eval(t: Treee, env: Environment): Int = t match {  // pattern matching sull’albero t --> la def controlla se l'albero t è un "Sum", se non c'è match passa al sotto albero "Var" e ancora se non c'è match arriva al sotto albero "Const"
    case Sum(l, r) => eval(l, env) + eval(r, env) //il valore della somma di due espressioni è pari alla somma dei valori delle loro espressioni; il valore di una variabile è ottenuto direttamente dall’environment; il valore di una costante è la costante stessa.
    case Var(n) => env(n)
    case Const(v) => v
  }

  def main(args: Array[String]) {
    //(x+x)+(7+y) expression = 5 + 5 + 3(const) + 7
    val exp: Treee = Sum(Sum(Var("x"), Var("x")), Sum(Const(3), Var("y")))
    val env: Environment = { case "x" => 5 case "y" => 7} // x := 5, y := 7, const := 3
    println("Expression: " + exp)
    println("Evaluation with x=5, y=7: " + eval(exp, env))
  }
}

//altro esempio match
object esempiomesi {
  val i : Int = 1

  def esmpiomesi1 (args: Array[String]){
    i match {
      case 1 => println("Jan")
      case 2 => println("Feb")
      case _ => println("Invalid Month")
    }}}

// L’idea alla base del pattern matching è quella di eseguire il match di un valore con una serie di pattern e,
// non appena il match è trovato, estrarre e nominare varie parti del valore per valutare il codice che ne fa uso.

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// TRAIT
// Una classe in Scala oltre che poter ereditare da una super-classe può anche importare del codice da uno o più trait.
// per i programmatori Java il modo più semplice per capire cosa sono i trait è concepirli come interfacce che possono contenere del codice.
// In Scala quando una classe eredita da un trait ne implementa la relativa interfaccia ed eredita tutto il codice contenuto in essa.

// Per comprendere a pieno l’utilità dei trait osserviamo un classico esempio: gli oggetti ordinati.
// Si rivela spesso utile riuscire a confrontare oggetti di una data classe con se stessi, ad esempio per ordinarli.


trait Ord {
  def < (that: Any): Boolean  //minore
  def <=(that: Any): Boolean =  (this < that) || (this == that) //minore uguale
  def > (that: Any): Boolean = !(this <= that) //maggiore
  def >=(that: Any): Boolean = !(this < that) //maggiore uguale
}

object Datee {
  def main(args: Array[String]) {
    class Date(y: Int, m: Int, d: Int) extends Ord {   //"extends Ord" che segue il nome della classe e dei parametri, dichiara che la classe "Date" eredita il codice dal trait "extends Ord"
      def year = y
      def month = m
      def day = d
      override def toString(): String = year + "-" + month + "-" + day

      // Il tipo Any usato precedentemente è il super-tipo dati di tutti gli altri tipi in Scala. Può esser visto come una versione generica del tipo Object in Java dato che è altresì il super-tipo dei tipi base come Int, Float ecc.

      override def equals(that: Any): Boolean =   //L’implementazione di default del metodo "equals" non è utilizzabile perché, come in Java, confronta fisicamente gli oggetti.
        that.isInstanceOf[Date] && { // isInstanceOf, corrisponde all’operatore instanceOf di Java e restituisce true se e solo se l’oggetto su cui è applicato è una istanza del tipo dati
          val o = that.asInstanceOf[Date]  //asInstanceOf, corrisponde all’operatore di cast in Java: se l’oggetto è una istanza del tipo dati è visto come tale altrimenti viene sollevata un’eccezione ClassCastException.
          o.day == day && o.month == month && o.year == year
        }

      def <(that: Any): Boolean = {
        if (!that.isInstanceOf[Date])
          scala.sys.error("cannot compare " + that + " and a Date")  //L’ultimo metodo da definire è il predicato che testa la condizione di minoranza. Fa uso di un altro metodo predefinito, error, che solleva un’eccezione con il messaggio di errore specificato.

        val o = that.asInstanceOf[Date]
        (year < o.year) ||
          (year == o.year && (month < o.month ||
            (month == o.month && day < o.day)))
      }}}}

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//Programmazione Generica
// La programmazione generica riguarda la capacità di scrivere codice parametrizzato dai tipi.
// Per esempio un programmatore che scrive una libreria per le liste concatenate può incontrare il problema di
// decidere quale tipo dare agli elementi della lista.

// Scala rende possibile la definizione delle classi generiche (e metodi) per risolvere tale problema.
// Esaminiamo ciò con un esempio del più semplice container di classe possibile: un riferimento, che può essere o
// vuoto o un puntamento ad un oggetto di qualche tipo.

class Reference[T] {  //La classe Reference è parametrizzata da un tipo, chiamato T
  private var contents: T = _  //Questo tipo è usato nel corpo della classe come il tipo della variabile contents
  def set(value: T) { contents = value }
  def get: T = contents  // il tipo restituito dal metodo get
}

// Per usare la classe Reference è
// necessario specificare quale tipo usare per il tipo parametro T, il tipo di elemento contenuto dalla cella.
object IntegerReference {
  def main(args: Array[String]) {
    val cell = new Reference[Int]
    cell.set(13)
    println("Reference contains the half of " + (cell.get * 2))
  }
}
object IntegerReference2 {
  def main(args: Array[String]) {
    val cell2 = new Reference[Int]
    cell2.set(15)
    println("Reference contains the double of " + (cell2.get / 2))
  }
}

import scala.reflect.ClassTag
def evenElems[T: ClassTag]

